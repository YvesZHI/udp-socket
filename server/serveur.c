#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <netdb.h>
#include <time.h>

#ifdef __APPLE__
#include <unistd.h>
#include <arpa/inet.h>
#endif

int main(int argc, char ** argv)  
{  
	int sockfd;  
	socklen_t len;  
	char buf[255], buf2[255];  
	struct sockaddr_in adr_inet;  
	struct sockaddr_in adr_clnt;  

	// initialisation du serveur
	adr_inet.sin_family = AF_INET;  
	adr_inet.sin_port = htons(8888);  
	adr_inet.sin_addr.s_addr = htonl(INADDR_ANY);  
	len = sizeof(adr_clnt);  

	// creation d'un socket
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);  
	if (sockfd == -1)  
	{  
		fprintf(stderr, "Creation du socket erreur");  
		return 1;
	}  
	// bind
	if (bind(sockfd, (struct sockaddr *)&adr_inet, sizeof(adr_inet)) == -1)
	{  
		fprintf(stderr, "Bind erreur"); 
		return 1;
	} 

	struct hostent * he;
	struct in_addr ipv4addr;
	char * addr_client_ip;
	int port;

	while (1)  
	{
		// recevoir le message d'un client
		if (recvfrom(sockfd, buf, sizeof(buf), 0, (struct sockaddr *)&adr_clnt, &len) == -1)
		{  
			fprintf(stderr, "Reception du message erreur");  
			return 1;
		}
		// obtenir l'adresse du client en format de string
		addr_client_ip = inet_ntoa(adr_clnt.sin_addr);
		// obtenir la port du client
		port = adr_clnt.sin_port;
		printf("CLIENT: %s:%d", addr_client_ip, port);
		// convertir l'adresse ip en domaine si possible
		inet_pton(AF_INET, addr_client_ip, &ipv4addr);
		he = gethostbyaddr(&ipv4addr, sizeof(ipv4addr), AF_INET);
		if (he != NULL)
		{
			printf(" (%s)", he->h_name);
		}
		printf("\n");
		bzero(buf2, sizeof(buf2));
		// concatener "Bonjour " avec le message
		strcpy(buf2, buf);
		bzero(buf, sizeof(buf));
		strcat(buf, "Bonjour ");
		strcat(buf, buf2);
		// envoyer le message
		if (sendto(sockfd, buf, sizeof(buf), 0, (struct sockaddr *)&adr_clnt, len) == -1)
		{
			fprintf(stderr, "Envoie du message erreur");
		}
		bzero(buf, sizeof(buf));
	}  
	if (close(sockfd) == -1)
	{
		fprintf(stderr, "Fermeture du socket erreur");
	}
	return 0;  
}
