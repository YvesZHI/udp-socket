#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <netdb.h>

#ifdef __APPLE__
#include <unistd.h>
#include <arpa/inet.h>
#endif

#define BUFLEN 100


int main(int argc, char ** argv)
{  
	if (argc != 3)
	{
		fprintf(stderr, "Usage: ./client <adresse ip ou domaine> <port>\n");
		return 1;
	}
	char * addr_serv = argv[1];
	struct hostent * he;
	struct in_addr ** addr_list;
	// Si argv[1] est un domaine au lieu d'une adresse ip, on le convert en une adresse ip
	if ((he = gethostbyname(argv[1])) != NULL)
	{
		addr_list = (struct in_addr **)he->h_addr_list;
		addr_serv = inet_ntoa(*addr_list[0]);
	}
	int sockclient;
	struct sockaddr_in coord_serveur;
	int port;
	port = atoi(argv[2]);
	// preparation de la structure permettant de connecter le serveur
	socklen_t len = sizeof(coord_serveur);
	coord_serveur.sin_family = AF_INET;
	coord_serveur.sin_port = htons(port);
	coord_serveur.sin_addr.s_addr = inet_addr(addr_serv); 
	// creation d'un socket
	sockclient = socket(AF_INET, SOCK_DGRAM, 0);
	if (sockclient == -1)  
	{  
		fprintf(stderr, "Creation du socket erreur\n");
		exit(1);  
	}  
	char msgRecv[BUFLEN];
	char msgSent[BUFLEN];
	// entrer le message
	fgets(msgSent, BUFLEN, stdin);
	// envoyer le message
	if (sendto(sockclient, msgSent, strlen(msgSent), 0, (struct sockaddr *)&coord_serveur, len)==-1)
	{
		fprintf(stderr, "Envoie du message erreur");
	}
	bzero(msgRecv, sizeof(msgRecv));
	// recevoir le message
	if (recvfrom(sockclient, msgRecv, BUFLEN, 0, (struct sockaddr *)&coord_serveur, &len) == -1)
	{
		fprintf(stderr, "Reception du message erreur");
	}
	printf("%s", msgRecv);

	if(close(sockclient) == -1)
	{
		fprintf(stderr, "Fermeture du socket erreur");
	}

	return 0;
}
